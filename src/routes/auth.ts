import { Response, Request } from "express";
import { login, registerEmployee, registerWorker } from '../http/controllers/auth';

var express = require('express');
var router = express.Router();

router.post('/login', (req:Request, res:Response) => login(req, res));

router.post('/worker/register', (req:Request, res:Response) => registerWorker(req, res));

router.post('/employee/register', (req:Request, res:Response) => registerEmployee(req, res));


module.exports = router;
