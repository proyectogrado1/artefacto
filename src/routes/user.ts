import { Request, Response } from "express";
import { getBalance } from "../http/controllers/user";
import { checkIsWorker } from "../http/middlewares/user";

const express = require('express');
const router = express.Router();
const passport = require('passport');

router.use(passport.authenticate('jwt', { session: false }));

router.get('/balance', checkIsWorker, (req:Request, res:Response) => getBalance(req, res));


module.exports = router;
