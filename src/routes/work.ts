import { Request, Response } from "express";
import { createWork, getWorkWithoutDetails, 
        listAvailableWorks, applyWorkerToWork, checkIfUserAppliedToWork,
        listWorksApplied, 
        listWorksCreated,
        listWorkersAppliedByWork,
        selectWorkerToWork,
        listWorksCurrentlyWorkingOfWorkers,
        listWorkOfEmployerInProgress,
        setFinishWork } from "../http/controllers/work";
import { checkIsEmployeer, checkIsWorker } from "../http/middlewares/user";

const express = require('express');
const router = express.Router();
const passport = require('passport');

router.use(passport.authenticate('jwt', { session: false }));

router.put('/create', checkIsEmployeer, (req:Request, res:Response) => createWork(req, res));
router.get('/list', (req:Request, res:Response) => listAvailableWorks(req, res));
router.get('/applied', checkIsWorker, (req:Request, res:Response) => listWorksApplied(req, res));
router.get('/created', checkIsEmployeer, (req:Request, res:Response) => listWorksCreated(req, res));

router.get('/worker/working', checkIsWorker, (req:Request, res:Response) => listWorksCurrentlyWorkingOfWorkers(req, res));
router.get('/employer/inprogress', checkIsEmployeer, (req:Request, res:Response) => listWorkOfEmployerInProgress(req, res));

router.get('/:id', (req:Request, res:Response) => getWorkWithoutDetails(req, res));
router.post('/:id/worker/apply', checkIsWorker, (req:Request, res:Response) => applyWorkerToWork(req, res));
router.get('/:id/worker/check', checkIsWorker, (req:Request, res:Response) => checkIfUserAppliedToWork(req, res));
router.get('/:id/workers', checkIsEmployeer, (req:Request, res:Response) => listWorkersAppliedByWork(req, res));
router.post('/:id/select', checkIsEmployeer, (req:Request, res:Response) => selectWorkerToWork(req, res));
router.post('/:id/finish', checkIsEmployeer, (req:Request, res:Response) => setFinishWork(req, res));

module.exports = router;
