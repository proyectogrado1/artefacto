import { Request, Response } from "express";
import { listCategories } from "../http/controllers/category";

const express = require('express');
const router = express.Router();

router.get('/list', (req:Request, res:Response) => listCategories(req, res));

module.exports = router;
