import { NextFunction, Request, Response } from "express";
import * as StatusCodes from 'http-status';
import { ERRORS } from "../../shared/json/errors";

export const checkIsEmployeer = (req: Request|any, res: Response, next:NextFunction) => {
    const user = req.user;
    if(user?.is_employeer){
        return next();
    }
    else
        return res.status(StatusCodes.UNAUTHORIZED).json(ERRORS.DONT_USER_EMPLOYEE);
}

export const checkIsWorker = (req: Request|any, res: Response, next:NextFunction) => {
    const user = req.user;
    if(user?.is_worker){
        return next();
    }
    else
        return res.status(StatusCodes.UNAUTHORIZED).json(ERRORS.DONT_USER_WORKER);
}