import { db } from "../../config/database";
import { COLLECTIONS } from "../../shared/json/collections";
import { ObjectId } from "mongodb"

const passport = require('passport');
const JwtStrategy = require('passport-jwt').Strategy;
const ExtractJwt = require('passport-jwt').ExtractJwt;

let opts:any = {
    jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
    secretOrKey: 'secret'
}

passport.use(new JwtStrategy(opts, async (jwt_payload:any, done:any) => {
    const db_conn = await db();
    const user = await db_conn.collection(COLLECTIONS.USERS).findOne({"_id" : new ObjectId(jwt_payload.data._id)});
    if (user) 
        return done(null, user);
    else 
        return done(null, false);
}));