import { Request, Response } from "express";
import jwt from 'jsonwebtoken';
import Joi from 'joi';
import { createEmployee, createWorker, findByDocument, findByEmail } from '../../services/user';
import bcrypt from 'bcrypt';
import { LoginType, UserType } from '../../shared/json/types';
import * as StatusCodes from 'http-status';
import { ERRORS } from "../../shared/json/errors";

export const login = async (req:Request, res:Response) => {
    try{
        const data:LoginType = req.body;

        const user:any = await findByEmail(data.email);        
        if(!user)
            return res.status(StatusCodes.FORBIDDEN).json(ERRORS.USER_OR_PASSWORD_INVALID);

        if(await bcrypt.compare(data.password, user.password)){
            const token = jwt.sign({
                exp: Math.floor(Date.now() / 1000) + (60 * 60 * 60),
                data: user
            }, 'secret');

            delete user.password;
        
            return res.status(StatusCodes.OK).json({
                user: user,
                token: token
            })
        }

        return res.status(StatusCodes.UNAUTHORIZED).json(ERRORS.USER_OR_PASSWORD_INVALID)
    }
    catch(e: any){
        return res.status(StatusCodes.INTERNAL_SERVER_ERROR).json();
    }
}

export const registerWorker = async (req:Request, res:Response): Promise<any> => {
    try{
        const data:UserType = req.body;

        const validator = Joi.object().keys({
            email: Joi.string().email().required(),
            password: Joi.string().required(),
            name: Joi.string().required(),
            lastname: Joi.string().required(),
            document: Joi.string().required(),
            document_type: Joi.string().required(),
            country: Joi.string().required(),
            providence: Joi.string().required(),
            city: Joi.string().required(),
            neighborhood: Joi.string().required(),
            address: Joi.string().required(),
            phone: Joi.string().required()
        });
        
        const validationResult = validator.validate(data);

        if(validationResult.error){
            return res.status(StatusCodes.BAD_REQUEST).json({
                ...ERRORS.VALIDATORS,
                validators: validationResult.error.details
            })
        }

        const checkExistUserWithEmail = await findByEmail(data.email);
        if(checkExistUserWithEmail){
            return res.status(StatusCodes.CONFLICT).json(ERRORS.EMAIL_USED)
        }

        const checkExistUserWithDocument = await findByDocument(data.document, data.document_type);
        if(checkExistUserWithDocument){
            return res.status(StatusCodes.CONFLICT).json(ERRORS.DOCUMENT_USED)
        }

        data.password = bcrypt.hashSync(data.password, 10);

        const user = await createWorker(data);

        return res.status(StatusCodes.CREATED).json(user)
    }
    catch(e: any){
        if(e.name === "SequelizeUniqueConstraintError"){
            return res.status(StatusCodes.BAD_REQUEST).json({
                details :{
                    type: `${e.errors[0].path}.unique` ,
                    path: [
                        e.errors[0].path
                    ]
                }
            });
        }

        return res.status(StatusCodes.INTERNAL_SERVER_ERROR).json(e);
    }
}

export const registerEmployee = async (req:Request, res:Response): Promise<any> => {
    try{
        const data:UserType = req.body;

        const validator = Joi.object().keys({
            email: Joi.string().email(),
            password: Joi.string(),
            name: Joi.string(),
            lastname: Joi.string(),
            document: Joi.string(),
            document_type: Joi.string(),
            country: Joi.string(),
            providence: Joi.string(),
            city: Joi.string(),
            neighborhood: Joi.string(),
            address: Joi.string(),
            phone: Joi.string()
        });
        
        const validationResult = validator.validate(data);

        if(validationResult.error){
            return res.status(StatusCodes.BAD_REQUEST).json({
                ...ERRORS.VALIDATORS,
                validators: validationResult.error.details
            })
        }

        const checkExistUserWithEmail = await findByEmail(data.email);
        if(checkExistUserWithEmail){
            return res.status(StatusCodes.CONFLICT).json(ERRORS.EMAIL_USED)
        }

        const checkExistUserWithDocument = await findByDocument(data.document, data.document_type);
        if(checkExistUserWithDocument){
            return res.status(StatusCodes.CONFLICT).json(ERRORS.DOCUMENT_USED)
        }

        data.password = bcrypt.hashSync(data.password, 10);

        const user = await createEmployee(data);

        return res.status(StatusCodes.CREATED).json(user)
    }
    catch(e: any){
        return res.status(StatusCodes.INTERNAL_SERVER_ERROR);
    }
}