import { Request, Response } from "express";
import * as StatusCodes from 'http-status';
import * as CategoryService from "../../services/category";

export const listCategories = async (req:Request|any, res:Response) => {
    try{
        const categories = await CategoryService.getCategories();
        return res.status(StatusCodes.OK).json(categories)
    }
    catch(e: any){
        return res.status(StatusCodes.INTERNAL_SERVER_ERROR).json();
    }
}