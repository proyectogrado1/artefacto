import { Request, Response } from "express";
import * as StatusCodes from 'http-status';
import { getBalanceUser } from "../../services/balance";

export const getBalance = async (req:Request|any, res:Response) => {
    try{
        const balance = await getBalanceUser(req.user)
        return res.status(StatusCodes.OK).json(balance)
    }
    catch(e: any){
        return res.status(StatusCodes.INTERNAL_SERVER_ERROR).json();
    }
}
