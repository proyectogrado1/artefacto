import { Request, Response } from "express";
import * as StatusCodes from 'http-status';
import * as WorkService from "../../services/work";
import { ErrorResponse } from "../../shared/common/error-response";

export const createWork = async (req:Request|any, res:Response) => {
    try{
        const balance = await WorkService.createWork(req.user, req.body)
        return res.status(StatusCodes.OK).json(balance)
    }
    catch(e: any){
        return res.status(StatusCodes.INTERNAL_SERVER_ERROR).json();
    }
}

export const listAvailableWorks = async (req:Request|any, res:Response) => {
    try{
        const is_per_hours = req.query?.is_per_hours == "true" ? true : false
        const balance = await WorkService.getAvailableWorks(req.query?.category, is_per_hours)
        return res.status(StatusCodes.OK).json(balance);
    }
    catch(e: any){
        return res.status(StatusCodes.INTERNAL_SERVER_ERROR).json();
    }
}

export const getWorkWithoutDetails = async (req:Request|any, res:Response) => {
    try{
        const balance = await WorkService.getWorkWithoutDetails(req.params.id);
        return res.status(StatusCodes.OK).json(balance);
    }
    catch(e: any){
        return res.status(StatusCodes.INTERNAL_SERVER_ERROR).json();
    }
}

export const applyWorkerToWork = async (req:Request|any, res:Response) => {
    try{
        const apply = await WorkService.workerApply(req.user, req.params.id, req.body.comments);
        return res.status(StatusCodes.OK).json(apply);
    }
    catch(e: any){
        console.log(e);
        if(e instanceof ErrorResponse){
            res.status(e.status).json({
                message: e.message,
                code: e.code
            });
            return;
        }
        res.status(StatusCodes.INTERNAL_SERVER_ERROR).send();
        return;
    }
}

export const checkIfUserAppliedToWork = async (req:Request|any, res:Response) => {
    try{
        const check = await WorkService.checkIfUserAppliedToWork(req.user, req.params.id);
        return res.status(StatusCodes.OK).json(check);
    }
    catch(e: any){
        console.log(e);
        if(e instanceof ErrorResponse){
            res.status(e.status).json({
                message: e.message,
                code: e.code
            });
            return;
        }
        res.status(StatusCodes.INTERNAL_SERVER_ERROR).send();
        return;
    }
}

export const listWorksApplied = async (req:Request|any, res:Response) => {
    try{
        const works = await WorkService.getWorksApplied(req.user);
        return res.status(StatusCodes.OK).json(works);
    }
    catch(e: any){
        console.log(e);
        if(e instanceof ErrorResponse){
            res.status(e.status).json({
                message: e.message,
                code: e.code
            });
            return;
        }
        res.status(StatusCodes.INTERNAL_SERVER_ERROR).send();
        return;
    }
}

export const listWorksCreated = async (req:Request|any, res:Response) => {
    try{
        const works = await WorkService.getWorksCreated(req.user);
        return res.status(StatusCodes.OK).json(works);
    }
    catch(e: any){
        console.log(e);
        if(e instanceof ErrorResponse){
            res.status(e.status).json({
                message: e.message,
                code: e.code
            });
            return;
        }
        res.status(StatusCodes.INTERNAL_SERVER_ERROR).send();
        return;
    }
}

export const listWorkersAppliedByWork = async (req:Request|any, res:Response) => {
    try{
        const workers = await WorkService.getWorkersAppliedToWork(req.params.id);
        return res.status(StatusCodes.OK).json(workers);
    }
    catch(e: any){
        console.log(e);
        if(e instanceof ErrorResponse){
            res.status(e.status).json({
                message: e.message,
                code: e.code
            });
            return;
        }
        res.status(StatusCodes.INTERNAL_SERVER_ERROR).send();
        return;
    }
}

export const selectWorkerToWork = async (req:Request|any, res:Response) => {
    try{
        const response = await WorkService.selectWorkerToWork(req.params.id, req.body.id_worker);
        return res.status(StatusCodes.OK).json(response);
    }
    catch(e: any){
        console.log(e);
        if(e instanceof ErrorResponse){
            res.status(e.status).json({
                message: e.message,
                code: e.code
            });
            return;
        }
        res.status(StatusCodes.INTERNAL_SERVER_ERROR).send();
        return;
    }
}

export const listWorkOfEmployerInProgress = async (req:Request|any, res:Response) => {
    try{
        const response = await WorkService.getWorksInProgress(req.user);
        return res.status(StatusCodes.OK).json(response);
    }
    catch(e: any){
        console.log(e);
        if(e instanceof ErrorResponse){
            res.status(e.status).json({
                message: e.message,
                code: e.code
            });
            return;
        }
        res.status(StatusCodes.INTERNAL_SERVER_ERROR).send();
        return;
    }
}

export const listWorksCurrentlyWorkingOfWorkers = async (req:Request|any, res:Response) => {
    try{
        const response = await WorkService.getWorksSelectedCurrently(req.user);
        return res.status(StatusCodes.OK).json(response);
    }
    catch(e: any){
        console.log(e);
        if(e instanceof ErrorResponse){
            res.status(e.status).json({
                message: e.message,
                code: e.code
            });
            return;
        }
        res.status(StatusCodes.INTERNAL_SERVER_ERROR).send();
        return;
    }
}

export const setFinishWork = async (req:Request|any, res:Response) => {
    try{
        const response = await WorkService.finishWork(req.params.id, parseInt(req.body.amount));
        return res.status(StatusCodes.OK).json(response);
    }
    catch(e: any){
        console.log(e);
        if(e instanceof ErrorResponse){
            res.status(e.status).json({
                message: e.message,
                code: e.code
            });
            return;
        }
        res.status(StatusCodes.INTERNAL_SERVER_ERROR).send();
        return;
    }
}