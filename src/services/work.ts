import { ObjectID } from 'bson';
import httpStatus from 'http-status';
import { FindOptions } from 'mongodb';
import { db } from '../config/database'
import { ErrorResponse } from '../shared/common/error-response';
import { COLLECTIONS } from '../shared/json/collections';
import { ERRORS } from '../shared/json/errors';
import { UserType, WorkType } from '../shared/json/types';

//@ts-ignore
const WORKS_STATUS = {
    "CREATED": "created",
    "EVALUATING_APPLICANTS": "evaluating_applicants"
}

export const createWork = async (employer:UserType, work_data: WorkType) => {
    const db_conn = await db();

    let body:any = {
        employer: {
            _id: employer._id,
            name: employer.name,
            lastname: employer.lastname,

        },
        title: work_data.title,
        range_time: work_data.range_time,
        date_start: work_data.date_start,
        date_end: work_data.date_end, 
        is_per_hours: work_data.is_per_hours,
        price_per_hour: work_data.price_per_hour,
        price_total: work_data.price_total,
        description: work_data.description,
        created_at: new Date().getTime(),
        updated_at: new Date().getTime(),
        applicants: [],
        workers_selected: [],
        is_visible: work_data.is_visible,
        status: work_data.status,
        finished: false,
        country: work_data.country,
        providence: work_data.providence,
        city: work_data.city,
        neighborhood: work_data.neighborhood,
        address: work_data.address,
        category: work_data.category,
        abilities: work_data.abilities
    }

    const work = await db_conn.collection(COLLECTIONS.WORKS).insertOne(body);

    return {
        id: work.insertedId,
        ...body
    }
}

export const getAvailableWorks = async (category:string, is_per_hours:boolean) => {
    console.log(is_per_hours)
    const db_conn = await db();

    const filter:any = { is_visible: true, finished: false };

    if(category)
        filter.category = category;

    if(is_per_hours != undefined)
        filter.is_per_hours = is_per_hours;

    const options:FindOptions = { sort: { updated_at: -1 }, projection: { address: 0, neighborhood: 0, workers_selected: 0, applicants: 0} };
    
    const works = await db_conn.collection(COLLECTIONS.WORKS).find(filter, options).toArray();
    
    return works;
}

export const getWorkWithoutDetails = async (_id: string) => {
    const db_conn = await db();

    const filter:any = { _id: new ObjectID(_id) };
    const options:FindOptions = { sort: { updated_at: -1 }, projection: { address: 0, neighborhood: 0, workers_selected: 0, applicants: 0} };
    
    const works = await db_conn.collection(COLLECTIONS.WORKS).findOne(filter, options);
    
    return works;
}

export const getWorkersAppliedToWork = async (_id: string) => {
    const db_conn = await db();

    const filter:any = { _id: new ObjectID(_id) };
    const options:FindOptions = { sort: { updated_at: -1 }, projection: { workers_selected: 1, applicants: 1 } };
    
    const workers_applicants:any = await db_conn.collection(COLLECTIONS.WORKS).findOne(filter, options);

    const options_worker:FindOptions = { projection: { _id: 1,  email: 1, name: 1, lastname: 1, phone: 1 } };

    let applicants = [];
    for(let applicant of workers_applicants.applicants){
        let worker_data:any = await db_conn.collection(COLLECTIONS.USERS).findOne({ _id: applicant._id_worker}, options_worker);
        if(worker_data){
            worker_data.info_apply = applicant;
            applicants.push(worker_data);
        }
    }

    let workers_selected = [];
    for(let worker of workers_applicants.workers_selected){
        const worker_data:any = await db_conn.collection(COLLECTIONS.USERS).findOne({ _id: worker._id_worker}, options_worker);
        workers_selected.push(worker_data);
    }

    return {applicants, workers_selected};
}

export const selectWorkerToWork = async (_id_work: string, _id_worker: string) => {
    const db_conn = await db();

    const filter = { _id: new ObjectID(_id_work) };
    const work:any = await db_conn.collection(COLLECTIONS.WORKS).findOne(filter, {});
    const apply_date = new Date().getTime();

    const filter_worker = { _id: new ObjectID(_id_worker) };
    const worker:any = await db_conn.collection(COLLECTIONS.USERS).findOne(filter_worker, {});

    let workers_selected = work.workers_selected || [];

    workers_selected.push({
        _id_worker: worker._id,
        apply_date
    })

    await db_conn.collection(COLLECTIONS.WORKS).updateOne(filter, {
        $set: { 
            workers_selected: workers_selected,
            is_visible: false,
            status: "in_progress"
        }
    });

    let works_accepted = worker.works_accepted || [];

    works_accepted.push({
        _id_work: work._id,
        apply_date
    })

    await db_conn.collection(COLLECTIONS.USERS).updateOne(filter_worker, {
        $set: { works_accepted : works_accepted }
    });

    return {
        _id_worker: _id_worker,
        apply_date
    };
}

export const workerApply = async (worker:UserType, _id_work: string, comments: string) => {
    const db_conn = await db();
    const filter:any = { _id: new ObjectID(_id_work) };
    const work:any = await db_conn.collection(COLLECTIONS.WORKS).findOne(filter, {});

    let applicants: any[] = work.applicants || [];

    //Check if user again apply, not allowed in case
    const user_already_applied = applicants.find((applicant) => applicant._id_worker.toString() == worker._id?.toString())
    if(user_already_applied){
        throw new ErrorResponse(httpStatus.CONFLICT, ERRORS.USER_ALREADY_APPLIED_TO_WORD.message, ERRORS.USER_ALREADY_APPLIED_TO_WORD.code);
    }

    const apply_date = new Date().getTime();

    //Add applicant to work
    applicants.push({
        _id_worker: worker._id,
        apply_date: apply_date,
        comments: comments
    });

    await db_conn.collection(COLLECTIONS.WORKS).updateOne(filter, {
        $set: { applicants: applicants }
    });

    //Add work to applied list
    let works_applied: any[] = worker.works_applied || [];
    works_applied.push({
        _id_work: new ObjectID(_id_work),
        apply_date: apply_date,
        comments: comments
    });

    await db_conn.collection(COLLECTIONS.USERS).updateOne({ _id: worker._id }, {
        $set: { works_applied: works_applied }
    });

    return {
        _id_worker: worker._id,
        apply_date: new Date().getTime(),
        comments: comments
    };
}

export const checkIfUserAppliedToWork = async (worker:UserType, _id_work: string) => {
    const db_conn = await db();

    const worker_data:any = await db_conn.collection(COLLECTIONS.USERS).findOne({ _id: worker._id }, {});
    
    const works_applied: any[] = worker_data.works_applied || [];
    const works_accepted: any[] = worker_data.works_accepted || [];

    const check = works_applied.find(work => work._id_work.toString() == _id_work);
    const selected = works_accepted.find(work => work._id_work.toString() == _id_work);

    let response:any = {
        check: check ? true: false
    }

    if(selected){
        const work:any = await db_conn.collection(COLLECTIONS.WORKS).findOne({ _id: new ObjectID(_id_work) }, {})
        const employer:any = await db_conn.collection(COLLECTIONS.USERS).findOne({ _id: work.employer._id }, {});
        response.selected = {
            "employer": employer.name+" "+employer.lastname,
            "email": employer.email,
            "city": employer.city,
            "address": employer.address,
            "phone": employer.phone,
            "work": {
                "city": work.city,
                "address": work.address,
            }
        }
    }

    return response;
}

export const getWorksApplied = async (worker:UserType) => {
    const db_conn = await db();

    const worker_data:any = await db_conn.collection(COLLECTIONS.USERS).findOne({ _id: worker._id }, {});
    
    const works_applied: any[] = worker_data.works_applied || [];
    
    const works_applied_data: any[] = [];

    const options:FindOptions = { projection: { address: 0, neighborhood: 0, workers_selected: 0, applicants: 0} };


    for(let work_applied of works_applied){
        const work_data:any = await db_conn.collection(COLLECTIONS.WORKS).findOne({ _id: work_applied._id_work }, options);
        work_applied.work_data = work_data;
        works_applied_data.push(work_applied);
    }

    return works_applied_data;
}

export const getWorksCreated = async (employer:UserType) => {
    const db_conn = await db();

    const works:any = await db_conn.collection(COLLECTIONS.WORKS).find({ 'employer._id': employer._id }, {sort: { updated_at: -1 } }).toArray();

    return works;
}

export const getWorksInProgress = async (employer:UserType) => {
    const db_conn = await db();

    const filter:any = { 'employer._id': employer._id, status: 'in_progress'};
    const options:FindOptions = { sort: { updated_at: -1 } };

    const works:any[] = await db_conn.collection(COLLECTIONS.WORKS).find(filter, options).toArray();

    const response = [];
    
    for(let work of works){
        let info_work = {   
            "_id": work._id,
            "title": work.title,
            "worker": "N/D",
            "is_per_hours": work.is_per_hours,
            "price_per_hour": work.price_per_hour,
            "price_total": work.price_total
        }

        if(work.workers_selected[0]){
            const worker_data:any = await db_conn.collection(COLLECTIONS.USERS).findOne({ _id: work.workers_selected[0]._id_worker }, {});
            info_work.worker = worker_data.name+" "+worker_data.lastname
        }

        response.push(info_work);
    } 

    return response;
}

export const getWorksSelectedCurrently = async (worker:UserType) => {
    const db_conn = await db();
    const filter:any = { _id: worker._id};

    const worker_data:any = await db_conn.collection(COLLECTIONS.USERS).findOne(filter, {});
    const response = [];

    for(let work_accepted of worker_data.works_accepted){
        const work_data:any = await db_conn.collection(COLLECTIONS.WORKS).findOne({ _id: work_accepted._id_work, status: "in_progress" }, {});
        if(work_data){
            const employer_data:any = await db_conn.collection(COLLECTIONS.USERS).findOne({ _id: work_data.employer._id }, {});

            let info_work = {
                "_id": work_data._id,
                "title": work_data.title,
                "employer": employer_data.name+" "+employer_data.lastname,
                "is_per_hours": work_data.is_per_hours,
                "price_per_hour": work_data.price_per_hour,
                "price_total": work_data.price_total
            }
            response.push(info_work);
        }
    }

    return response;
}

export const finishWork = async (_id_work: string, amount:number) => {
    const db_conn = await db();

    const filter = { _id: new ObjectID(_id_work) };
    const work:any = await db_conn.collection(COLLECTIONS.WORKS).findOne(filter, {});

    await db_conn.collection(COLLECTIONS.WORKS).updateOne(filter, {
        $set: { 
            is_visible: false,
            status: "finished"
        }
    });

    const worker_selected = work.workers_selected[0];
    const worker_balance:any = await db_conn.collection(COLLECTIONS.BALANCE)
                                .findOne({ _id: worker_selected._id_worker }, {});

    await db_conn.collection(COLLECTIONS.BALANCE).updateOne({ _id: worker_selected._id_worker }, {
        $set: { 
            balance: (worker_balance.balance + amount)
        }
    });

    return {
        _id_work,
        amount
    }
}

