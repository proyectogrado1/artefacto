import { db } from '../config/database'
import { COLLECTIONS } from '../shared/json/collections';
import { ObjectId } from "mongodb"
import { UserType } from '../shared/json/types';

export const getBalanceUser = async (user: UserType) => {
    const db_conn = await db();
    const balance:any = await db_conn.collection(COLLECTIONS.BALANCE).findOne({ _id: new ObjectId(user._id) });

    console.log(balance)
    if(balance){
        return {
            balance: balance.balance,
            iso: "COP"
        };
    }   
    else{
        db_conn.collection(COLLECTIONS.BALANCE).insertOne({ _id: new ObjectId(user._id), balance: 0, iso: "COP"});
        return {
            balance: 0.0,
            iso: "COP"
        }
    }
}