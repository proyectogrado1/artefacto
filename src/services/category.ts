import { db } from '../config/database'
import { COLLECTIONS } from '../shared/json/collections';

export const getCategories = async () => {
    const db_conn = await db();
    const categories:any = await db_conn.collection(COLLECTIONS.CATEGORIES).find().toArray();

    return categories;
}