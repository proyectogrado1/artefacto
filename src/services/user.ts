import { db } from '../config/database'
import { COLLECTIONS } from '../shared/json/collections';
import { UserType } from '../shared/json/types';


export const findByEmail = async (email: string) => {
    const db_conn = await db();
    const user:any = await db_conn.collection(COLLECTIONS.USERS).findOne({ email: email });
    return user;
}

export const findByDocument = async (document: string, document_type: string) => {
    const db_conn = await db();
    const user = await db_conn.collection(COLLECTIONS.USERS).findOne({ document: document, document_type: document_type });
    return user;
}

export const createWorker = async (userData: UserType) => {
    const db_conn = await db();

    let body:any = {
        email: userData.email,
        password: userData.password,
        name: userData.name,
        lastname: userData.lastname,
        is_worker: true,
        is_employeer: false,
        document: userData.document,
        document_type: userData.document_type,
        country: "CO",
        providence: userData.providence,
        city: userData.city,
        neighborhood: userData.neighborhood,
        address: userData.address,
        phone: userData.phone
    }

    const user = await db_conn.collection(COLLECTIONS.USERS).insertOne(body);
    delete body.address;

    return {
        id: user.insertedId,
        ...body
    }
}

export const createEmployee = async (userData: UserType) => {
    const db_conn = await db();

    let body:any = {
        email: userData.email,
        password: userData.password,
        name: userData.name,
        lastname: userData.lastname,
        is_worker: false,
        is_employeer: true,
        document: userData.document,
        document_type: userData.document_type,
        country: "CO",
        providence: userData.providence,
        city: userData.city,
        neighborhood: userData.neighborhood,
        address: userData.address,
        phone: userData.phone
    }

    const user = await db_conn.collection(COLLECTIONS.USERS).insertOne(body);
    delete body.address;

    return {
        id: user.insertedId,
        ...body
    }
}