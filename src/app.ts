import { NextFunction, Request, Response } from "express";
import './http/middlewares/passport';
import cors from 'cors';
var createError = require('http-errors');
var express = require('express');
var logger = require('morgan');
var path = require('path');
var app = express();

app.use(cors({ origin: '*' }));
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'public')));

require("dotenv").config({path: path.join(__dirname, '../.env')});

// view engine setup
// app.set('views', path.join(__dirname, 'views')); 
// app.set('view engine', 'ejs');  

app.use('/v1', require('./routes/auth'));
app.use('/v1/user', require('./routes/user'));
app.use('/v1/work', require('./routes/work'));
app.use('/v1/category', require('./routes/category'));

// catch 404 and forward to error handler
app.use(function(req:Request, res:Response, next:NextFunction) {
  next(createError(404));
});

// error handler
app.use(function(err:any, req:Request, res:Response, next:NextFunction) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

const port = process.env.PORT || 8080
app.listen(port, () => console.log('App is running'))
