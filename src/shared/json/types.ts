export type UserType = {
    _id?: string,
    email: string,
    password: string,
    name: string,
    lastname: string,
    document: string,
    document_type: string,
    country: string,
    providence: string,
    city: string,
    neighborhood: string,
    address: string,
    phone: string,
    works_applied: any
}

export type LoginType = {
    email: string,
    password: string
}

export type WorkType = {
    _id?: string,
    _id_employer: string | undefined,
    title: string,
    range_time: boolean,
    date_start?: number,
    date_end?: number, 
    is_per_hours: boolean,
    price_per_hour?: number,
    price_total?: number,
    description: string,
    is_visible: boolean,
    status: string,
    finished: boolean,
    country: string,
    providence: string,
    city: string,
    neighborhood: string,
    address: string,
    created_at: number,
    updated_at: number,
    applicants: ApplicantType[],
    workers_selected: ApplicantType[],
    category: string,
    abilities: string[]
}

export type ApplicantType = {
    _id_worker: string,
    aplication_date: number
}
