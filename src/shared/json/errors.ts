export const ERRORS = {
    INVALID_TOKEN : {
        message: "Invalid Token",
        code: "invalid_token"
    },
    EMAIL_USED : {
        message: "Email has already been used",
        code: "email_used"
    },
    EMAIL_REQUIRED : {
        message: "Email is required",
        code: "email_required"
    },
    USER_HAS_ALREADY_REGISTERED : {
        message: "The user has already registered",
        code: "user_has_already_registered"
    },
    USER_OR_PASSWORD_INVALID : {
        message: "The user or password is invalid",
        code: "user_or_password_invalid"
    },
    DOCUMENT_USED : {
        message: "The document has already been used",
        code: "document_used"
    },
    VALIDATORS : {
        message: "Validation errors",
        code: "validators"
    },
    DONT_USER_EMPLOYEE : {
        message: "The user isn't a employee",
        code: "dont_user_employee"
    },
    DONT_USER_WORKER : {
        message: "The user isn't a worker",
        code: "dont_user_worker"
    },
    USER_ALREADY_APPLIED_TO_WORD : {
        message: "The User already applied to this work",
        code: "user_already_applied_to_word"
    }
}