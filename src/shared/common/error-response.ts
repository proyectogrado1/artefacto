export class ErrorResponse extends Error {
    status:number;
    message:string;
    error:any;
    code:string;
    validators?:any;

    constructor(status:number, message:string, code:string, error?:string, validators?:string){
        super(message);
        
        this.status = status;
        this.code = code;
        this.message = message;
        this.error = error;
        this.validators = validators
    }
}